const { expect } = require("chai");
const chai = require("chai"); //similar to import in java
const chaiHttp = require("chai-http");
chai.use(chaiHttp); // chai-http is a plugin to chai so we need to use chai.use()
const consts = require("../consts/const.json");

// mocha suite
//Similar like TestNG to hold all the test suite
describe("Get call suite", ()=> {
  // mocha test
  it("Get first call", async ()=> {
    // getting details of post 1
    const response = await chai.request(consts.baseUrl).get("posts/1");
    // status must be 200
    expect(response.status).to.equal(200);
    // response body id must be 1
    expect(response.body.id).to.equal(1);
    // checking response title
    expect(response.body.title).to.equal(
      "sunt aut facere repellat provident occaecati excepturi optio reprehenderit"
    );
    //Checking Response body
    expect(response.body.body).to.equal(
      "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
    );
  });
});
