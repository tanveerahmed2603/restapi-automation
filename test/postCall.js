const { expect } = require("chai");
const chai = require("chai"); //similar to import in java
const chaiHttp = require("chai-http");
chai.use(chaiHttp); // chai-http is a plugin to chai so we need to use chai.use()
const consts = require("../consts/const.json");

// mocha suite
describe("post call suite", ()=> {
  const { key, value } = consts.header;
  const payload = {
    title: "foo1",
    body: "bar1",
    userId: 100932,
  };

  it("Checking post status code", async function () {
    const response = await chai
      .request(consts.baseUrl)
      .post("posts")
      .set(key, value)
      .send(payload);

    // status must be 201
    expect(response.status).to.equal(201);
  });
  // mocha test
  it("checking post call", async function () {
    const response = await chai
      .request(consts.baseUrl)
      .post("posts")
      .set(key, value)
      .send(payload);

    const { title, body, userId, id } = response.body;

    // checking response against payload
    expect(title).to.equal(payload.title);
    expect(body).to.equal(payload.body);
    expect(userId).to.equal(payload.userId);
    expect(id).to.equal(101);
  });
});
