const { expect } = require("chai");
const chai = require("chai"); 
const chaiHttp = require("chai-http");
chai.use(chaiHttp); // chai-http is a plugin to chai so we need to use chai.use()
const consts = require("../consts/const.json");

// mocha suite
describe("post and put call suite", ()=> {
  // combining post and put call together so that response from POST call I can pass in request to PUT call
  // mocha test
  const { key, value } = consts.header;
  it("post and pull call", async ()=> {
    let response;
    const postPayload = {
      title: "foo1",
      body: "bar1",
      userId: 100932,
    };
    // adding one resource to server with above payload using post method
    response = await chai
      .request(consts.baseUrl)
      .post("posts")
      .set(key,value)
      .send(postPayload);
    // response status must be 201
    expect(response.status).to.equal(201);
    const { title, body, userId, id } = response.body;
    // checking response details must be similar to what I am passing
    expect(title).to.equal(postPayload.title);
    expect(body).to.equal(postPayload.body);
    expect(userId).to.equal(postPayload.userId);
    expect(id).to.equal(101);
    const putPayload = {
      id: id,
      title: "fooUpdated",
      body: "barUpdated",
      userId: 301,
    };
    // calling the put request for id which getting from post call
    response = await chai
      .request(consts.baseUrl)
      .put("posts/1")
      .set(key, value)
      .send(putPayload);
    // checking response against payload
    expect(response.status).to.equal(200);
    expect(response.body.id).to.equal(1);
    expect(response.body.title).to.equal(putPayload.title);
    expect(response.body.body).to.equal(putPayload.body);
    expect(response.body.userId).to.equal(putPayload.userId);
  });
});
