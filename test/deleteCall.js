const { expect } = require("chai");
const chai = require("chai"); //similar to import in java
const chaiHttp = require("chai-http");
chai.use(chaiHttp); // chai-http is a plugin to chai so we need to use chai.use()
const consts = require("../consts/const.json");

// mocha suite
describe("Delete call suite", ()=> {
  // mocha test
  it("delete call", async ()=> {
    // deleting posts with id 1
    const response = await chai.request(consts.baseUrl).delete("posts/1");
    // checking response status must be 200
    expect(response.status).to.equal(200);
  });
});
