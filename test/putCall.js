const { expect } = require("chai");
const chai = require("chai"); //similar to import in java
const chaiHttp = require("chai-http");
chai.use(chaiHttp); // chai-http is a plugin to chai so we need to use chai.use()
const consts = require("../consts/const.json");

// mocha suite
describe("Put call suite", ()=> {
  // mocha test
  const { key, value } = consts.header;
  it("put call", async function () {
    const payload = {
      id: 1,
      title: "fooUpdated",
      body: "barUpdated",
      userId: 1,
    };
    // calling put call with above payload
    const response = await chai
      .request(consts.baseUrl)
      .put("posts/1")
      .set(key, value)
      .send(payload);
    // status code must be 200
    expect(response.status).to.equal(200);

    const { title, body, userId, id } = response.body;
    // checking response against payload
    expect(id).to.equal(payload.id);
    expect(title).to.equal(payload.title);
    expect(body).to.equal(payload.body);
    expect(userId).to.equal(payload.userId);
  });
});
