## API Automation TEST

In this project, I used the Mocha, chai framework in Javascript to automate API testing for the site "jsonplaceholder.typicode.com." 

## Concepts Included

* Make GET, POST, PUT, and DELETE calls
* Set headers for a request
* Set the body for a request
* Assert the response from a request
* Use the response from one call in the request for another
* Mocha testing
* Docker Image
* GitLab CI/CD pipeline

## Tools

* Mocha
* Javascript
* Chai
* Npm package manager
* Docker
* GitLab CI/CD

## Requirements

In order to utilise this project you need to have the following installed locally:

* npm install 
* node.js
* VS Code
* Docker
* GitLab

## Usage

1. [Clone this project] (git@gitlab.com:tanveerahmed2603/restapi-automation.git)

2. Install all required dependencies - `npm install`

3. To run all tests in project - `npm run test`

4. To run any specific file (getCall.js) - `npm run test test/getCall.js`

5. To run tests parallely - `npm test parallelTest`

6. Run Docker compose up to run locally- ` docker-compose up`

## Reporting

*NOTE*:
## Post MVP
Reports for each module are not showing on log files. It's work on progress.